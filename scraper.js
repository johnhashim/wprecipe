let cheerio = require('cheerio')
let fetch = require('node-fetch')

let url = 'https://blog.myfitnesspal.com/instant-pot-chickpea-tikka-masala/'



async function start() {
    let html = await fetchPageSource(url)

    const $ = cheerio.load(html)

    let stars = $('.star-result').text()
    let count = $('.star-result + .count').text()

    console.log(stars)
    console.log(count)
}


async function fetchPageSource(myurl) {

    let html = await fetch(myurl)
        .then(res => res.text())
    // .then(text => console.log(text))
    return html
}

start()


