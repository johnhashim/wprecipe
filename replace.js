
let firebase = require('firebase');
let firebaseConfig = {
    apiKey: "AIzaSyDTWKRqBfuLHRo6vHeWZOCaj7Ohm4Ow6eo",
    authDomain: "mfp-blog-recipes.firebaseapp.com",
    databaseURL: "https://mfp-blog-recipes.firebaseio.com",
    projectId: "mfp-blog-recipes",
    storageBucket: "mfp-blog-recipes.appspot.com",
    messagingSenderId: "685622046792",
    appId: "1:685622046792:web:91b008c458267b2f"
};

let app = firebase.initializeApp(firebaseConfig);
var db = app.firestore();

const rootURL = 'https://dev1mfp.wpengine.com/wp-json/wp/v2/posts/'


async function getContent(recipe, data, docId) {
    let content;
    let posturl = rootURL + docId;
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Authorization", "Basic WmFjaCBSdXNzZWxsOnloQ3ogc2xrQyBkVWRsIFoyNVcgZW5ZeSA0U2N3");

    var requestOptions = {
        method: 'PUT',
        headers: myHeaders,
        redirect: 'follow'
    };

    await fetch(posturl, requestOptions)
        .then(response => response.json())
        .then(result => content = result.content.raw)
        .catch(error => {
          db.collection('recipes').doc(docId).update({
            stagingError: true,
          })
          console.log(`${docId}: ${error}`)
        });

    let contentClean = await cleanContent(data["recipeId"], content);
    return contentClean;

}

async function cleanContent(recipeId, oldContent) {
    let cleanedContent = oldContent.replace(/\[print_this\][\s\S]*?\[\/print_this\]/, `[print_this][wprm-recipe id="${recipeId}"][/print_this]`);
    return cleanedContent;
}



async function updateContent(cleanContent, postID) {
    let body = {
        content: cleanContent
    }
    let posturl = rootURL + postID;
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Authorization", "Basic WmFjaCBSdXNzZWxsOnloQ3ogc2xrQyBkVWRsIFoyNVcgZW5ZeSA0U2N3");

    var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        redirect: 'follow',
        body: JSON.stringify(body)
    };

    await fetch(posturl, requestOptions)
        .then(response => response.json())
        .then(console.log(`updated recipe ${postID}`))
        .then(db.collection('recipes').doc(postID).update({
          stagingSuccess: true,
        }))
        .catch(error => console.log(`${postID}: ${error}`));

}

async function main() {
    db.collection('recipes').get()
        .then(querySnapshot => {
            //querySnapshot.forEach(doc => console.log(doc.data()))
            errorCount = 0;
            let docs = []
            querySnapshot.forEach(async (doc) => {
                docs.push(doc)


            });
            async function proccessDocs() {


                for (const doc of docs) {
                    
                    await processDoc(doc)
                    await wait(100)
                    // console.log('Waited .5s')
                }
            }
            proccessDocs()

            async function processDoc(doc) {
                var recipe = new Object();
                let data = doc.data()
                if (data.error) {
                        return;
                    }
                    if (!data.recipeId) {
                    return
                    }
                    if (data.stagingError || data.stagingSuccess) {
                      console.log(doc.id)
                    return;
                    }
                let contentToUpdate = await getContent(recipe, data, doc.id)
                await updateContent(contentToUpdate, doc.id)
            }
        })

    async function wait(ms) {
        return new Promise(resolve => {
            setTimeout(resolve, ms);
        });
    }

}


main()





































