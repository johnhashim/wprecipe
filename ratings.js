const csv = require('csv');
var fs = require('fs');
const http = require('http');
const fastcsv = require('fast-csv')
const ws = fs.createWriteStream("export.csv")
const obj = csv();

var ratings = [];
obj.from.path('./ratings.csv').to.array(function (data) {
    for (var index = 0; index < data.length; index++) {
        // if (index > 20) {
        //     break
        // }
        if (index === 0) {
            continue
        }

        let cleanRow = data[index]
        if (cleanRow[15]) {
            let dataRemoved = cleanRow[4] + ' ' + cleanRow[5] + ' ' + cleanRow[6] + ' ' + cleanRow[7]
            cleanRow.splice(4, 4)
            cleanRow[3] = cleanRow[3] + ' ' + dataRemoved

        } else if (cleanRow[14]) {
            let dataRemoved = cleanRow[4] + ' ' + cleanRow[5] + ' ' + cleanRow[6]
            cleanRow.splice(4, 3)
            cleanRow[3] = cleanRow[3] + ' ' + dataRemoved


        } else if (cleanRow[13]) {
            let dataRemoved = cleanRow[4] + ' ' + cleanRow[5]
            cleanRow.splice(4, 2)
            cleanRow[3] = cleanRow[3] + ' ' + dataRemoved



        } else if (cleanRow[12]) {
            let dataRemoved = cleanRow[4]
            cleanRow.splice(4, 1)
            cleanRow[3] = cleanRow[3] + ' ' + dataRemoved
        }
        ratings.push({
            ...cleanRow
        });
    }
    console.log(ratings);
    //console.log(ratings.length)
    fastcsv
        .write(ratings)
        .pipe(ws)
});
const server = http.createServer(function (req, resp) {
    resp.writeHead(200, { 'content-type': 'application/json' });
    resp.end(JSON.stringify(ratings));
});
server.listen(8080);