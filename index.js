let firebase = require('firebase');
let firebaseConfig = {
    apiKey: "AIzaSyDTWKRqBfuLHRo6vHeWZOCaj7Ohm4Ow6eo",
    authDomain: "mfp-blog-recipes.firebaseapp.com",
    databaseURL: "https://mfp-blog-recipes.firebaseio.com",
    projectId: "mfp-blog-recipes",
    storageBucket: "mfp-blog-recipes.appspot.com",
    messagingSenderId: "685622046792",
    appId: "1:685622046792:web:91b008c458267b2f"
};

let app = firebase.initializeApp(firebaseConfig);
var db = app.firestore();
const { promisify } = require('util');
const setTimeoutAsync = promisify(setTimeout);

const rootURL = 'https://dev1mfp.wpengine.com/wp-json/wp/v2/';


//this is default vals

function setDefaultVals(recipe, data) {
    recipe['servings'] = data["recipeYieldClean"]
    recipe['date'] = data['date']
    recipe['cook_time'] = data['cookTimeClean']
    recipe['name'] = data['postTitle']
    recipe['prep_time'] = data['prepTimeClean']
    recipe['total_time'] = data['totalTimeClean']
    recipe['summary'] = data['recipeDescription']
}


//this are loop through instructions
function parseSteps(recipe, data) {
    let instructions = [{ instructions: [], name: data['postTitle'] }]
    data["stepsClean"].forEach(step => {
        instructions[0].instructions.push({ text: step, image: 0 })
    })
    recipe["instructions"] = instructions
}

// looping through ingridients
function parseIngredients(recipe, data) {
    let ingredients = []
    data['ingredientsClean'].forEach(group => {
        let ing = {
            ingredients: [],
            name: '',
        };
        ing.name = (group.groupName != "Group Name") ? group.groupName : data["postTitle"]
        ing.ingredients = group.items.map(item => {

            let ingredient = {
                unit: item.ingredientUnit,
                amount: item.ingredientQuantity,
                name: item.ingredientName,
                notes: item.ingredientNotes

            }
            if (item.ingredientMetricQuantity && item.ingredientMetricUnit) {
                ingredient.converted = {
                    2: {
                        amout: item.ingredientMetricQuantity,
                        unit: item.ingredientMetricUnit
                    }
                }
            }

            return ingredient;



        })
        ingredients.push(ing)
    })
    recipe["ingredients"] = ingredients
    //console.log(JSON.stringify(recipe))
}

// nutritions
function parseNutritions(recipe, data) {
    let nutrition = {
        calories: data.calories,
        carbohydrates: data.carbs,
        fat: data.fat,
        protein: data.protein
    }
    recipe["nutrition"] = nutrition
}

// get keywords
function parseTags(recipe, data) {
    if (!data.keywords) {
        // db.collection('recipes').doc(data.id).update({ importIssueFlag: true, keywordsError: true })
        return
    }
    let tags = {
    }
    tags["keyword"] = data['keywords'].split(',').map(keyword => {
        kwdata = []
        kwdata = keyword
        return kwdata
    })
    return recipe["tags"] = tags

}


// get the recipe thumbnail

async function getFeaturedMedia(postId) {
    let featured_image;
    let featured_image_url;
    let posturl = `${rootURL}posts/${postId}`;
    await fetch(posturl)
        .then(res => res.json())
        .then(json => {
            featured_image = json.featured_media
            featured_image_url = json.jetpack_featured_media_url
        })
    return [await featured_image, await featured_image_url]

}
async function perseFeaturedMedia(recipe, data) {
    let [imageId, imageUrl] = await getFeaturedMedia(data['id'])
    recipe['image_id'] = await imageId
    // recipe['image_url'] = await imageUrl
}

// async function parsePermalink(postId) {
//     let permalink;
//     let posturl = `https://blog.myfitnesspal.com/wp-json/wp/v2/posts/${postId}`;
//     await fetch(posturl)
//         .then(res => res.json())
//         .then(json => {
//             permalink = json.link
//         })
//     return await permalink
// }



// let cheerio = require('cheerio')
// let fetch = require('node-fetch')

// async function parseStars(permalink) {
//     let rating = {}
//     let html = await fetchPageSource(permalink)
//     const $ = cheerio.load(html)

//     let stars = $('.star-result').text().trim()
//     let count = $('.star-result + .count').text().trim().replace(/[\])}[{(]/g, '');
//     rating["reviewValue"] = stars
//     rating["reviewCount"] = count
//     return rating

// }


// async function fetchPageSource(myurl) {

//     let html = await fetch(myurl)
//         .then(res => res.text())
//     // .then(text => console.log(text))
//     return html
// }





// post recipe methods
function createRecipeInWordPress(recipe, postID) {
    let url = `${rootURL}wprm_recipe`;
    fetch(url, {

        method: 'POST', //
        credentials: 'include',
        headers: new Headers({
            'Authorization': 'Basic WmFjaCBSdXNzZWxsOnloQ3ogc2xrQyBkVWRsIFoyNVcgZW5ZeSA0U2N3"',
            'Content-Type': 'application/json; charset=utf-8'
        }),
        credentials: 'include',
        mode: 'cors',
        body: JSON.stringify(recipe),
    })
        .then((response) => response.json())
        .then((json) => {
            // console.log(json)
            // console.log(postID)
            db.collection('recipes').doc(postID).update({
                recipeId: json["id"]

            }).catch(err => {
                console.log(err);
                console.log(json)
                console.log(postID)
            });
            console.log(`Created Recipe for Post ${postID}`)
        })
        .catch((err, response) => {
            console.log(`Error with Recipe ${postID}`)
            // debugger;
            // console.log(err)
        }
        );
}





db.collection('recipes').get()
    .then(querySnapshot => {
        //querySnapshot.forEach(doc => console.log(doc.data()))
        errorCount = 0;
        let docs = []
        querySnapshot.forEach(async (doc) => {
            docs.push(doc)

        });
        async function proccessDocs() {


            for (const doc of docs) {
                await processDoc(doc)
                await wait(1500)
                // console.log('Waited .5s')
            }
        }
        proccessDocs()
        // console.log(docs.length)
        // for (let i = 0; i < docs.length; i++) {
        //     console.log('Processing Doc')
        //     setTimeoutAsync(processDoc(docs[i]), 500)

        // }

        async function processDoc(doc) {
            var recipe = new Object();
            let data = doc.data()

            // let permalink;
            if (data.error || data.flag || data.johnFlag) return;
            // console.log(data.id)

            if (!data.stepsClean) {
                console.log('Recipe Steps Error')
                // db.collection('recipes').doc(data.id).update({ importIssueFlag: true, stepsError: true })
                // errorCount++;
                return
            }


            setDefaultVals(recipe, data)
            await perseFeaturedMedia(recipe, data)
            parseSteps(recipe, data)
            parseIngredients(recipe, data)
            parseNutritions(recipe, data)
            parseTags(recipe, data)

            // permalink = await parsePermalink(data.id)
            // let rating = await parseStars(permalink)
            // console.log(`Permalink: ${permalink}`)

            let postBody = {
                "recipe": recipe,
            }

            createRecipeInWordPress(postBody, doc.id)
            return;
        }
        // querySnapshot.forEach(
        //     setTimeout(async function (doc) {


        //     })


        //     //console.log(JSON.stringify(postBody))
        // )
        console.log(`${errorCount} unfinished Recipe posts`)
    })

async function wait(ms) {
    return new Promise(resolve => {
        setTimeout(resolve, ms);
    });
}


