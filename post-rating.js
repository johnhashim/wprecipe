
let firebase = require('firebase');
let firebaseConfig = {
    apiKey: "AIzaSyDTWKRqBfuLHRo6vHeWZOCaj7Ohm4Ow6eo",
    authDomain: "mfp-blog-recipes.firebaseapp.com",
    databaseURL: "https://mfp-blog-recipes.firebaseio.com",
    projectId: "mfp-blog-recipes",
    storageBucket: "mfp-blog-recipes.appspot.com",
    messagingSenderId: "685622046792",
    appId: "1:685622046792:web:91b008c458267b2f"
};

let app = firebase.initializeApp(firebaseConfig);
var db = app.firestore();

const csv = require('csv');
var fs = require('fs');
var randomip = require('random-ip');
const obj = csv();

function parseCsv() {

    obj.from.path('./export.csv').to.array(async function (data) {
        for (var index = 0; index < data.length; index++) {
            // if (index > 3) {
            //     break
            // }
            let recipeId = await getRecipe(data[index][2])
            let scoreRating = data[index][4]
            let randomIp = randomip('192.168.2.0', 24)
            postRating(recipeId, scoreRating, randomIp)

        }

    });

}

async function getRecipe(postId) {
    let recipeId;
    await db.collection('recipes').doc(postId).get()
        .then(doc => {
            //console.log(doc.data()["recipeId"])
            recipeId = doc.data()["recipeId"]
        })
    return recipeId
}
function postRating(id, score, ip) {
    body = {
        rating: {
            recipe_id: id,
            user_id: 0,
            ip: ip,
            rating: score
        }
    }
    let url = 'http://wprecipe.wordpress.test/wp-json/wp-recipe-maker/v1/rating/';
    fetch(url, {

        method: 'POST', //
        credentials: 'include',
        headers: new Headers({
            'Authorization': 'Basic ' + Buffer.from('john:pABr 4kyr brzR cD5K ZrBu y2n8').toString('base64'),
            'Content-Type': 'application/json; charset=utf-8'
        }),
        credentials: 'include',
        mode: 'cors',
        body: JSON.stringify(body),
    })
        .then((response) => response.json())
        .then((recipe) => {
            console.log('Success:', body);
        })
        .catch((error) => {
            console.error('Error:', error);
        });
}

parseCsv()



